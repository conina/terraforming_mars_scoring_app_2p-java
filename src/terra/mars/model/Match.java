package terra.mars.model;

public class Match {

	private Result resultOne;
	private Result resultTwo;

	
	public Match (Result result1, Result result2) {
		resultOne = result1;
		resultTwo = result2;
	}
	
	public Match () {
		resultOne = new Result();
		resultTwo = new Result();
	}
	
	
	public Result getResultOne () {
		return resultOne;
	}
	
	public void setResultOne (Result resultOne) {
		this.resultOne = resultOne;
	}
	
	public Result getResultTwo () {
		return resultTwo;
	}
	
	public void setResultTwo (Result resultTwo) {
		this.resultTwo = resultTwo;
}
	

}