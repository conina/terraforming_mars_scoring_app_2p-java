package terra.mars.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Result {

	private final IntegerProperty terraforming;
	private final IntegerProperty  awards;
	private final IntegerProperty  milestones;
	private final IntegerProperty  map;
	private final IntegerProperty  cards;
	private final StringProperty playerName;
	private final IntegerProperty id;

	public Result() {

		this.terraforming = new SimpleIntegerProperty(0);
		this.awards = new SimpleIntegerProperty(0);
		this.milestones = new SimpleIntegerProperty(0);
		this.map = new SimpleIntegerProperty(0);
		this.cards = new SimpleIntegerProperty(0);
		this.playerName = new SimpleStringProperty("Name");
		this.id = new SimpleIntegerProperty(0);
	}

	public Result (String playerName) {
		this.terraforming = new SimpleIntegerProperty(0);
		this.awards = new SimpleIntegerProperty(0);
		this.milestones = new SimpleIntegerProperty(0);
		this.map = new SimpleIntegerProperty(0);
		this.cards = new SimpleIntegerProperty(0);
		this.playerName = new SimpleStringProperty(playerName);
		this.id = new SimpleIntegerProperty(0);

	}

	public int getTerraforming () {
		return terraforming.get();
	}

	public void setTerraforming (int terraforming) {
		this.terraforming.set(terraforming);
	}

	public IntegerProperty terraformingProperty() {
		return terraforming;
	}

	public int getAwards() {
		return awards.get();
	}

	public void setAwards(int awards) {
		this.awards.set(awards);
	}

	public IntegerProperty awardsProperty() {
		return awards;
	}

	public int getMilestones() {
		return milestones.get();
	}

	public void setMilestones(int milestones) {
		this.milestones.set(milestones);
	}

	public IntegerProperty milestonesProperty() {
		return milestones;
	}

	public int getMap() {
		return map.get();
	}

	public void setMap(int map) {
		this.map.set(map);
	}

	public IntegerProperty mapProperty() {
		return map;
	}

	public int getCards() {
		return cards.get();
	}

	public void setCards(int cards) {
		this.cards.set(cards);
	}

	public IntegerProperty cardsProperty () {
		return cards;
	}

	public int getTotal() {		
		return terraforming.get() + awards.get() + milestones.get() + map.get() + cards.get();

	}
	
	public String getPlayerName () {
		return this.playerName.get();
	}
	
	public void setPlayerName (String playerName) {
		this.playerName.set(playerName);
	}
	
	public StringProperty playerNameProperty () {
		return playerName;
	}
	
	public int getId() {
		return id.get();
	}

	public void setId (int id) {
		this.id.set(id);
	}

	public IntegerProperty idProperty () {
		return id;
	}

	public String toString() {
		return "Name: " + playerName.get() + "\nTerraforming: " + terraforming.get() + "\n" + 
				"Awards: " + awards.get() + "\n" + "Milestones: " + 
				milestones.get() + "\n" + "Map: " + map.get() + "\n" + 
				"Cards: " + cards.get() + "\n" + "Total: " + getTotal();
	}


}


