package terra.mars.view;

import terra.mars.MainApp;
import terra.mars.model.Match;
import terra.mars.model.Result;
import terra.mars.dbConnection;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

public class MatchOverviewControler {

	@FXML
	TableView<Match> matchTable;

	@FXML
	TableColumn<Match, String> PlayerOneColumn;

	@FXML
	TableColumn<Match, String> PlayerTwoColumn;

	@FXML
	private Label namePlayerOne;
	@FXML
	private Label terraformingLabelPlayerOne;
	@FXML
	private Label awardsLabelPlayerOne;
	@FXML
	private Label milestonesLabelPlayerOne;
	@FXML
	private Label mapLabelPlayerOne;
	@FXML
	private Label cardsLabelPlayerOne;
	@FXML
	private Label totalLabelPlayerOne;


	@FXML
	private Label namePlayerTwo;
	@FXML
	private Label terraformingLabelPlayerTwo;
	@FXML
	private Label awardsLabelPlayerTwo;
	@FXML
	private Label milestonesLabelPlayerTwo;
	@FXML
	private Label mapLabelPlayerTwo;
	@FXML
	private Label cardsLabelPlayerTwo;
	@FXML
	private Label totalLabelPlayerTwo;

	private MainApp mainApp;


	//BarChart elements
	@FXML
	private CategoryAxis xAxis;
	@FXML
	private NumberAxis yAxis;
	@FXML
	private BarChart<?,?> barChart;

	private dbConnection conn= new dbConnection();
	
	public MatchOverviewControler () {
	}

	@FXML
	private void initialize() {


		PlayerOneColumn.setCellValueFactory(cellData-> cellData.getValue().getResultOne().playerNameProperty());
		PlayerTwoColumn.setCellValueFactory(cellData-> cellData.getValue().getResultTwo().playerNameProperty());

		showMatchDetail(null);

		/* add a listener to selection changes */

		matchTable.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue) -> showMatchDetail(newValue));
	}



	private void showMatchDetail(Match match) {

		// this is temporarily set to show only one result
		if (match != null) {
			namePlayerOne.setText(match.getResultOne().getPlayerName());
			terraformingLabelPlayerOne.setText(Integer.toString(match.getResultOne().getTerraforming()));
			awardsLabelPlayerOne.setText(Integer.toString(match.getResultOne().getAwards()));
			milestonesLabelPlayerOne.setText(Integer.toString(match.getResultOne().getMilestones()));
			mapLabelPlayerOne.setText(Integer.toString(match.getResultOne().getMap()));
			cardsLabelPlayerOne.setText(Integer.toString(match.getResultOne().getCards()));
			totalLabelPlayerOne.setText(Integer.toString(match.getResultOne().getTotal()));

			namePlayerTwo.setText(match.getResultTwo().getPlayerName());
			terraformingLabelPlayerTwo.setText(Integer.toString(match.getResultTwo().getTerraforming()));
			awardsLabelPlayerTwo.setText(Integer.toString(match.getResultTwo().getAwards()));
			milestonesLabelPlayerTwo.setText(Integer.toString(match.getResultTwo().getMilestones()));
			mapLabelPlayerTwo.setText(Integer.toString(match.getResultTwo().getMap()));
			cardsLabelPlayerTwo.setText(Integer.toString(match.getResultTwo().getCards()));
			totalLabelPlayerTwo.setText(Integer.toString(match.getResultTwo().getTotal()));

			updateChart(match);

		} else {
			namePlayerOne.setText("Player 1");
			terraformingLabelPlayerOne.setText("");
			awardsLabelPlayerOne.setText("");
			milestonesLabelPlayerOne.setText("");
			mapLabelPlayerOne.setText("");
			cardsLabelPlayerOne.setText("");
			totalLabelPlayerOne.setText("");

			namePlayerTwo.setText("Player 2");
			terraformingLabelPlayerTwo.setText("");
			awardsLabelPlayerTwo.setText("");
			milestonesLabelPlayerTwo.setText("");
			mapLabelPlayerTwo.setText("");
			cardsLabelPlayerTwo.setText("");
			totalLabelPlayerTwo.setText("");		
		}
	}

	@FXML
	private void handleNewMatch() {
		Match tempMatch = new Match();
		boolean okClicked = mainApp.showMatchEditDialog(tempMatch);
		if (okClicked) {
			mainApp.getMatchData().add(tempMatch);
			mainApp.putDataInDatabase(tempMatch);
		}
	}

	@FXML
	private void handleDeleteMatch() {
		Match matchtoBeDeleted = matchTable.getSelectionModel().getSelectedItem();

		if (matchtoBeDeleted != null) {
			matchTable.getItems().remove(matchtoBeDeleted);
			conn.removeMatchFromDB(matchtoBeDeleted);
			if (matchTable.getItems().isEmpty()) {
				barChart.getData().clear();
			}

		}else {
			// Nothing selected.
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(mainApp.getPrimaryStage());
			alert.setTitle("No Selection");
			alert.setHeaderText("No Person Selected");
			alert.setContentText("Please select a person in the table.");
			alert.showAndWait();
		}
	}

	
//	FIGURE OUT HOW TO EDIT DATA IN DATABASE 
//	- need o save the old match so we can find it in the database and then edit it
//	
	@FXML
	private void handleEditMatch() {
		Match matchtobeEdited = matchTable.getSelectionModel().getSelectedItem();
		
		if (matchtobeEdited != null) {
			boolean okClicked = mainApp.showMatchEditDialog(matchtobeEdited);
			if (okClicked) {
				showMatchDetail(matchtobeEdited);
				conn.editMatchInDB(matchtobeEdited);	
				
				
		}else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(mainApp.getPrimaryStage());
			alert.setTitle("No Selection");
			alert.setHeaderText("No Person Selected");
			alert.setContentText("Please select a person in the table.");
			alert.showAndWait();
		}
			
	}
	}

	public void setMainApp (MainApp mainApp) {
		this.mainApp = mainApp;
		matchTable.setItems(mainApp.getMatchData());

	}

	public void updateChart(Match match) {

		barChart.getData().clear();

		String result1Name = match.getResultOne().getPlayerName();
		Integer result1terraforming = match.getResultOne().getTerraforming();
		Integer result1awards = match.getResultOne().getAwards();
		Integer result1milestones = match.getResultOne().getMilestones();
		Integer result1map = match.getResultOne().getMap();
		Integer result1cards = match.getResultOne().getCards();
		Integer result1total = match.getResultOne().getTotal();


		String result2Name = match.getResultTwo().getPlayerName();
		Integer result2terraforming = match.getResultTwo().getTerraforming();
		Integer result2awards = match.getResultTwo().getAwards();
		Integer result2milestones = match.getResultTwo().getMilestones();
		Integer result2map = match.getResultTwo().getMap();
		Integer result2cards = match.getResultTwo().getCards();
		Integer result2total = match.getResultTwo().getTotal();


		XYChart.Series result1 = new XYChart.Series();
		result1.setName(result1Name);

		result1.getData().add(new XYChart.Data("Terraforming", result1terraforming));
		result1.getData().add(new XYChart.Data("Milestones"  , result1milestones));
		result1.getData().add(new XYChart.Data("Awards"  , result1awards));
		result1.getData().add(new XYChart.Data("Map"  , result1map));
		result1.getData().add(new XYChart.Data("Cards"  , result1cards));
		result1.getData().add(new XYChart.Data("Total"  , result1total));



		XYChart.Series result2 = new XYChart.Series();
		result2.setName(result2Name);

		result2.getData().add(new XYChart.Data("Terraforming", result2terraforming));
		result2.getData().add(new XYChart.Data("Milestones"  , result2milestones));
		result2.getData().add(new XYChart.Data("Awards"  , result2awards));
		result2.getData().add(new XYChart.Data("Map"  , result2map));
		result2.getData().add(new XYChart.Data("Cards"  , result2cards));
		result2.getData().add(new XYChart.Data("Total"  , result2total));



		//add data for Player One and Player two to the chart
		barChart.getData().add(result1);
		barChart.getData().add(result2);

	}
}
