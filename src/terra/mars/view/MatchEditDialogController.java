package terra.mars.view;

import terra.mars.MainApp;
import terra.mars.model.Match;
import terra.mars.model.Result;

import javafx.scene.control.TextField;


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class MatchEditDialogController {

	@FXML
	private TextField namePlayerOneField;	
	@FXML
	private TextField terraformingPlayerOneField;
	@FXML 
	private TextField awardsPlayerOneField;	
	@FXML
	private TextField milestonesPlayerOneField;
	@FXML
	private TextField mapPlayerOneField;
	@FXML 
	private TextField cardsPlayerOneField;


	@FXML
	private TextField namePlayerTwoField;
	@FXML
	private TextField terraformingPlayerTwoField;
	@FXML 
	private TextField awardsPlayerTwoField;
	@FXML
	private TextField milestonesPlayerTwoField;
	@FXML
	private TextField mapPlayerTwoField;
	@FXML 
	private TextField cardsPlayerTwoField;


	private Stage dialogStage;
	private Match match;
	private boolean okClicked = false;


	@FXML
	private void initialize() {
	}

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}


	public void setMatch(Match match) {
		this.match = match;
		
		namePlayerOneField.setText(match.getResultOne().playerNameProperty().getValue());
		terraformingPlayerOneField.setText(Integer.toString(match.getResultOne().getTerraforming()));
		awardsPlayerOneField.setText(Integer.toString(match.getResultOne().getAwards()));
		milestonesPlayerOneField.setText(Integer.toString(match.getResultOne().getMilestones()));
		mapPlayerOneField.setText(Integer.toString(match.getResultOne().getMap()));
		cardsPlayerOneField.setText(Integer.toString(match.getResultOne().getCards()));

		namePlayerTwoField.setText(match.getResultTwo().playerNameProperty().getValue());
		terraformingPlayerTwoField.setText(Integer.toString(match.getResultTwo().getTerraforming()));
		awardsPlayerTwoField.setText(Integer.toString(match.getResultTwo().getAwards()));
		milestonesPlayerTwoField.setText(Integer.toString(match.getResultTwo().getMilestones()));
		mapPlayerTwoField.setText(Integer.toString(match.getResultTwo().getMap()));
		cardsPlayerTwoField.setText(Integer.toString(match.getResultTwo().getCards()));

	}

	public boolean isOkClicked() {
		return okClicked;
	}

	@FXML
	private void handleOk() {
		if (isInputValid()) {

			match.getResultOne().setPlayerName(namePlayerOneField.getText());
			match.getResultOne().setTerraforming(Integer.parseInt(terraformingPlayerOneField.getText()));
			match.getResultOne().setAwards(Integer.parseInt(awardsPlayerOneField.getText()));
			match.getResultOne().setMilestones(Integer.parseInt(milestonesPlayerOneField.getText()));
			match.getResultOne().setMap(Integer.parseInt(mapPlayerOneField.getText()));
			match.getResultOne().setCards(Integer.parseInt(cardsPlayerOneField.getText()));

			match.getResultTwo().setPlayerName(namePlayerTwoField.getText());
			match.getResultTwo().setTerraforming(Integer.parseInt(terraformingPlayerTwoField.getText()));
			match.getResultTwo().setAwards(Integer.parseInt(awardsPlayerTwoField.getText()));
			match.getResultTwo().setMilestones(Integer.parseInt(milestonesPlayerTwoField.getText()));
			match.getResultTwo().setMap(Integer.parseInt(mapPlayerTwoField.getText()));
			match.getResultTwo().setCards(Integer.parseInt(cardsPlayerTwoField.getText()));
			okClicked = true;
			dialogStage.close();
			
		}
	}

	@FXML
	private void handleCancel() {
		dialogStage.close();
	}
	
	public boolean isInteger( String input )
	{
	   try
	   {
	      Integer.parseInt( input );
	      return true;
	   }
	   catch( Exception e )
	   {
	      return false;
	   }
	}

	private boolean isInputValid() {
		String errorMessage = "";

		//checking input for player one
		if (namePlayerOneField.getText() == null || namePlayerOneField.getText().length() == 0 )  {
			errorMessage += "No valid name!\n"; 
		} 
		if (terraformingPlayerOneField.getText() == null || terraformingPlayerOneField.getText().length() == 0 || isInteger(terraformingPlayerOneField.getText()) == false) {
			errorMessage += "No valid input!"; 
		}
		if (awardsPlayerOneField.getText() == null || awardsPlayerOneField.getText().length() == 0 || isInteger(awardsPlayerOneField.getText()) == false) {
			errorMessage += "No valid input"; 
		}
		if (milestonesPlayerOneField.getText() == null || milestonesPlayerOneField.getText().length() == 0 || isInteger(milestonesPlayerOneField.getText()) == false) {
			errorMessage += "No valid input"; 
		} 
		if (mapPlayerOneField.getText() == null || mapPlayerOneField.getText().length() == 0 || isInteger(mapPlayerOneField.getText()) == false) {
			errorMessage += "No valid input"; 
		} 
		if (cardsPlayerOneField.getText() == null || cardsPlayerOneField.getText().length() == 0 || isInteger(cardsPlayerOneField.getText()) == false) {
			errorMessage += "No valid input"; 
		} 

		// checking input for player two
		if (namePlayerTwoField.getText() == null || namePlayerTwoField.getText().length() == 0) {
			errorMessage += "No valid name!\n"; 
		} 
		if (terraformingPlayerTwoField.getText() == null || terraformingPlayerTwoField.getText().length() == 0 || isInteger(terraformingPlayerTwoField.getText()) == false)  {
			errorMessage += "No valid input!"; 
		}
		if (awardsPlayerTwoField.getText() == null || awardsPlayerTwoField.getText().length() == 0 || isInteger(awardsPlayerTwoField.getText()) == false) {
			errorMessage += "No valid input"; 
		}
		if (milestonesPlayerTwoField.getText() == null || milestonesPlayerTwoField.getText().length() == 0 || isInteger(milestonesPlayerTwoField.getText()) == false) {
			errorMessage += "No valid input"; 
		} 
		if (mapPlayerTwoField.getText() == null || mapPlayerTwoField.getText().length() == 0 || isInteger(mapPlayerTwoField.getText()) == false)  {
			errorMessage += "No valid input"; 
		} 
		if (cardsPlayerTwoField.getText() == null || cardsPlayerTwoField.getText().length() == 0 || isInteger(cardsPlayerTwoField.getText()) == false)  {
			errorMessage += "No valid input"; 
		} 

		if (errorMessage.length() == 0) {
			return true;	
		} else {
			// Show the error message.
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(dialogStage);
			alert.setTitle("Invalid Fields");
			alert.setHeaderText("Please correct invalid fields");
			alert.setContentText(errorMessage);

			alert.showAndWait();

			return false;

		}
	}
}
