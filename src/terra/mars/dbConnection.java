package terra.mars;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import terra.mars.model.Match;
import terra.mars.model.Result;


public class dbConnection {

	public Statement stm = null;
	public Connection conn = null;
	private MainApp mainApp;

	public dbConnection () {

		// create connection
		try {

			//Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:terraDB.db");
			System.out.println ("Database opened/created.....");
			stm = conn.createStatement();

		} catch (Exception e) {
			System.out.println(e.getMessage());		
		}

	}

	public void createSchema() {

		try {
			String tableResult = "CREATE TABLE Result " + 
					"(id INTEGER not NULL, " + 
					" playerName VARCHAR(255), " +  
					" terraforming INTEGER, " +  
					" awards INTEGER, " + 
					" milestones INTEGER, " +
					" map INTEGER, " +
					" cards INTEGER, " +
					" PRIMARY KEY ( id ))";

			String tableMatch = "CREATE TABLE Match" +
					" (result_1 INTEGER, " +
					" result_2 INTEGER, " +
					" FOREIGN KEY (result_1) REFERENCES Result(id), " +
					"FOREIGN KEY (result_2) REFERENCES Result(id) ON DELETE CASCADE)";


			stm.executeUpdate(tableResult);
			stm.executeUpdate(tableMatch);


			System.out.println("Created schema");
		} catch (SQLException e) {
			System.out.println("Schema already created.");
		}
	}

	public void populateDatabase (Match match) {

		String result1Name = match.getResultOne().getPlayerName();
		Integer result1terraforming = match.getResultOne().getTerraforming();
		Integer result1awards = match.getResultOne().getAwards();
		Integer result1milestones = match.getResultOne().getMilestones();
		Integer result1map = match.getResultOne().getMap();
		Integer result1cards = match.getResultOne().getCards();

		String result2Name = match.getResultTwo().getPlayerName();
		Integer result2terraforming = match.getResultTwo().getTerraforming();
		Integer result2awards = match.getResultTwo().getAwards();
		Integer result2milestones = match.getResultTwo().getMilestones();
		Integer result2map = match.getResultTwo().getMap();
		Integer result2cards = match.getResultTwo().getCards();

		try {

			Integer player1ID = null;
			Integer player2ID = null;

			//putting data into Result table - Player1
			stm.execute("INSERT INTO Result(playerName, terraforming, awards, milestones, map, cards) "
					+ "VALUES('" + result1Name + "', " + result1terraforming + 
					"," + result1awards + "," + result1milestones + "," + 
					result1map + "," + result1cards +")");

			//getting the generated ID for Player1 and setting it into Result object
			ResultSet rs = stm.getGeneratedKeys();
			while (rs.next()) {
				player1ID = rs.getInt(1);
				System.out.println("Inserted ID of Player1 - " + player1ID);
				match.getResultOne().setId(player1ID);

			}
			//putting data into Result table - Player2
			stm.execute("INSERT INTO Result(playerName, terraforming, awards, milestones, map, cards) "
					+ "VALUES('" + result2Name  + "', " + result2terraforming + 
					"," + result2awards + "," + result2milestones + "," + 
					result2map + "," + result2cards +")");

			//getting the generated ID for Player2 and setting it into Result object
			rs = stm.getGeneratedKeys();
			while (rs.next()) {
				player2ID = rs.getInt(1);
				System.out.println("Inserted ID of Player2 - " + player2ID);
				match.getResultTwo().setId(player2ID);
			}

			//putting IDs of Player One and Player Two into Match Table
			stm.execute("INSERT INTO Match(result_1, result_2) VALUES(" + player1ID + "," + player2ID + ")");

		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}

	public void fetchDataFromDatabase(MainApp mainApp) {
		try {

			//fetching all data from database by joining Result table and Match table
			ResultSet rs = stm.executeQuery(""
					+ "SELECT r1.id AS id1, r1.playerName AS playerName1, r1.terraforming AS terraforming1, "
					+ "r1.awards AS awards1, r1.milestones AS milestones1, r1.map AS map1, \n" 
					+ "r1.cards AS cards1, \n" 
					+ "r2.id AS id2, r2.playerName AS playerName2, r2.terraforming AS terraforming2, "
					+ "r2.awards AS awards2, r2.milestones AS milestones2, r2.map AS map2, \n"
					+ "r2.cards AS cards2\n" 
					+ "FROM Match JOIN Result AS r1 JOIN Result AS r2 \n" 
					+ "ON Match.result_1 = r1.id AND Match.result_2 = r2.id");

			//creating two Result objects; one for player one, one for player two so that we can put them into match object and finally, into the observable list	
			while (rs.next()) {

				Result result1 = new Result();
				Result result2 = new Result();

				result1.setPlayerName(rs.getString("playerName1"));
				result1.setTerraforming(rs.getInt("terraforming1"));
				result1.setAwards(rs.getInt("awards1"));
				result1.setMilestones(rs.getInt("milestones1"));
				result1.setMap(rs.getInt("map1"));
				result1.setCards(rs.getInt("cards1"));
				result1.setId(rs.getInt("id1"));

				result2.setPlayerName(rs.getString("playerName2"));
				result2.setTerraforming(rs.getInt("terraforming2"));
				result2.setAwards(rs.getInt("awards2"));
				result2.setMilestones(rs.getInt("milestones2"));
				result2.setMap(rs.getInt("map2"));
				result2.setCards(rs.getInt("cards2"));
				result2.setId(rs.getInt("id2"));



				//put the data from Result1 and Result to into Match Object and then to Observable list in MainApp
				Match newMatchToAdd = new Match();
				newMatchToAdd.setResultOne(result1);
				newMatchToAdd.setResultTwo(result2);

				mainApp.getMatchData().add(newMatchToAdd);

			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());	
		}

	}

	public void removeMatchFromDB(Match match) {
		
		try {

			Integer result1ID = match.getResultOne().getId();
			Integer result2ID = match.getResultTwo().getId();


			System.out.println("This is result1 ID using resultID: " + result1ID);
			System.out.println("This is result2 ID: " + result2ID);


			// deleting Player One from Result table 
			stm.executeUpdate("DELETE FROM Result "
					+ "WHERE id = " + Integer.toString(result1ID));

			System.out.println("Deleted  ID from Result table: " + result1ID);


			// deleting Player Two from Result table 
			stm.executeUpdate("DELETE FROM Result "
					+ "WHERE id = " + Integer.toString(result2ID)); 

			System.out.println("Deleted  ID from Result table: " + result2ID);


			//deleting from Match table		
			stm.executeUpdate("DELETE FROM Match "
					+ "WHERE result_1 =" + Integer.toString(result1ID) + " AND result_2 = " + Integer.toString(result2ID));

			System.out.println("Deleted  IDs from Match table: " + result1ID + " " + result2ID);

		} catch (SQLException e) {
			System.out.println(e.getMessage());	
		}
	}

	public void editMatchInDB(Match match) {

		String result1Name = match.getResultOne().getPlayerName();
		Integer result1terraforming = match.getResultOne().getTerraforming();
		Integer result1awards = match.getResultOne().getAwards();
		Integer result1milestones = match.getResultOne().getMilestones();
		Integer result1map = match.getResultOne().getMap();
		Integer result1cards = match.getResultOne().getCards();
		Integer result1id = match.getResultOne().getId();

		String result2Name = match.getResultTwo().getPlayerName();
		Integer result2terraforming = match.getResultTwo().getTerraforming();
		Integer result2awards = match.getResultTwo().getAwards();
		Integer result2milestones = match.getResultTwo().getMilestones();
		Integer result2map = match.getResultTwo().getMap();
		Integer result2cards = match.getResultTwo().getCards();
		Integer result2id = match.getResultTwo().getId();

		System.out.println("Id of player two is " + result2id);

		//creating prepared statement for Player One
		String sql = "UPDATE Result SET playerName = ?, "
				+ "terraforming = ?,"
				+ "awards = ?,"
				+ "milestones = ?,"
				+ "map = ?,"
				+ "cards = ? "
				+ "WHERE id = ?";

		try {

			System.out.println ("Editing data in database");

			//updating information about Player One in DB
			PreparedStatement ps = conn.prepareStatement(sql);
			conn.setAutoCommit(false);

			ps.setString(1, result1Name);
			ps.setInt(2, result1terraforming);
			ps.setInt(3, result1awards);
			ps.setInt(4, result1milestones);
			ps.setInt(5, result1map);
			ps.setInt(6, result1cards);
			ps.setInt(7, result1id);

			ps.addBatch();

			//updating information about Player Two in DB
			ps.setString(1, result2Name);
			ps.setInt(2, result2terraforming);
			ps.setInt(3, result2awards);
			ps.setInt(4, result2milestones);
			ps.setInt(5, result2map);
			ps.setInt(6, result2cards);
			ps.setInt(7, result2id);

			ps.addBatch();


			ps.executeBatch();

			conn.commit();
			System.out.println ("Editing data in database finished");

		} catch (SQLException e) {
			System.out.println(e.getMessage());		
		}
	}
}