package terra.mars;

import java.io.IOException;
import java.sql.Connection;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import terra.mars.model.Match;
import terra.mars.view.MatchEditDialogController;
import terra.mars.view.MatchOverviewControler;
import terra.mars.model.Result;
import terra.mars.dbConnection;

import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

public class MainApp extends Application {

	private ObservableList<Match> matchData = FXCollections.observableArrayList();
	private Stage primaryStage;
	private dbConnection connection = new dbConnection();


	public ObservableList<Match> getMatchData() {
		return matchData;
	}

	@Override
	public void start(Stage primaryStage) {
		try {		
			// create connection with database
			connection.createSchema();

			//fetch data from DB
			connection.fetchDataFromDatabase(this);
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("view/MatchOverview.fxml"));
			Parent root = loader.load();

			Scene scene = new Scene(root, 1000, 400);

			primaryStage.setScene(scene);
			primaryStage.show();

			MatchOverviewControler controller = loader.getController();
			controller.setMainApp(this);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean showMatchEditDialog (Match match) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("view/MatchEditDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("New Match");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the person into the controller.
			MatchEditDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setMatch(match);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();
			return controller.isOkClicked();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void putDataInDatabase (Match match) {
		connection.populateDatabase(match); 
	}
	public Stage getPrimaryStage() {
        return primaryStage;
    }

	public static void main(String[] args) {
		launch(args);
	}
}
